function _twoDigits(number) {
    if(number < 10)
        return '0' + number
    else
        return number
}

export function formatTime(time) {
    const dateObject = new Date(time)
    let hours = dateObject.getMinutes()
    let minutes = dateObject.getSeconds()
    let seconds = Math.round(dateObject.getMilliseconds() / 100)

    return `${_twoDigits(hours)} : ${_twoDigits(minutes)} : ${_twoDigits(seconds)}`
}

let sequence = 0

export function setSequence(value) {
    sequence = value
}

export function nextId() {
    return ++sequence
}