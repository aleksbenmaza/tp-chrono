import React, {Component} from "react";

export default class TimerForm extends Component {

    state = {
        title: null,
        description: null,
        visible: false,
    }

    constructor() {
        super()
        this.titleRef = React.createRef()
        this.descriptionRef = React.createRef()
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.addTicket({
            title: this.state.title,
            description: this.state.description
        })
    }

    toggleVisible = () => this.setState({
        ...this.state,
        visible: !this.state.visible
    })

    render() {
        return (
            <div className="card col-md-4 mx-auto">
                {
                    this.state.visible &&
                    <form className="card-body"
                          onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="title">
                                Title
                            </label>
                            <input className="form-control"
                                   id="title"
                                   ref={this.titleRef}
                                   value={this.state.title}
                                   onChange={
                                       e => this.setState({
                                           ...this.state,
                                           title: e.target.value
                                       })
                                   }/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="description">
                                Description
                            </label>
                            <textarea className="form-control"
                                      id="description"
                                      ref={this.descriptionRef}
                                      value={this.state.description}
                                      onChange={
                                          e => this.setState({
                                              ...this.state,
                                              description: e.target.value
                                          })
                                      }></textarea>
                        </div>
                        <button className="btn btn-primary"
                                type="submit">
                            Add
                        </button>
                        <button className="btn btn-secondary"
                                onClick={this.toggleVisible}>
                            Cancel
                        </button>
                    </form>
                    ||
                    <button className="btn btn-outline-secondary col-md-2 mx-auto"
                            onClick={this.toggleVisible}>
                        +
                    </button>
                }
            </div>
        )
    }
}