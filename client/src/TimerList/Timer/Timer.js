import React, {Component} from "react";
import {formatTime} from "../../utils";

export default class Timer extends Component {

    state = {
        running: false,
        startTime: 0,
        currentTime: 0,
    }

    timeOffset = 0

    resume = () => {
        this.timeOffset = this.time

        this.intervalId = setInterval(
            () =>
                this.setState({
                    ...this.state,
                    startTime:
                        this.state.running ?
                            this.state.startTime :
                            Date.now(),
                    currentTime: Date.now() + (
                        this.timeOffset ?
                            this.timeOffset :
                            0
                    ),
                    running: true,
                })
            ,
            10
        )
    }

    stop = () => {
        clearInterval(this.intervalId)
        this.setState({
            ...this.state,
            running: false,
        })
    }

    reset = () => {
        this.timeOffset = 0
        this.setState({
            ...this.state,
            currentTime: Date.now(),
            startTime: Date.now()
        })
    }

    get time() {
        return this.state.currentTime -
               this.state.startTime
    }

    render() {
        return (
            <div>
                <div className="text-centered">
                    <h3>
                        {this.props.title}
                    </h3>
                    <em>
                        {this.props.description}
                    </em>
                    <h2>
                        {formatTime(this.time)}
                    </h2>
                </div>
                <div className="btn-group">
                    {
                        !this.state.running &&
                        <button className="btn btn-primary"
                                onClick={this.resume}>
                            {!this.time ? 'Start' : 'Resume'}
                        </button> ||
                        <button className="btn btn-outline-primary"
                                onClick={this.stop}>
                            Stop
                        </button>
                    }
                    {
                        this.time > 0 &&
                        <button className="btn btn-danger"
                                onClick={this.reset}>
                            Reset
                        </button>
                    }
                </div>
            </div>
        )
    }
}