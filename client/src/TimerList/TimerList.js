import React from "react"
import Timer from "./Timer/Timer";

export default function TimerList({timers}) {
    return (
        <ul className="list-group col-md-5 mx-auto">
        {
            timers.map(
                timer => (
                    <li key={timer.id}
                        className="list-group-item">
                        <Timer id={timer.id}
                               title={timer.title}
                               description={timer.description}/>
                    </li>
                )
            )
        }
        </ul>
    )
}